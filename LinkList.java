
class LinkList {

    Node head;
    int size;

    public LinkList() {
        this.head = null;
        this.size = 0;
    }

    public void insert(int dataNew) {
        Node newNode = new Node();
        newNode.data = dataNew;
        //menyambungkan newNode ke head
        newNode.next = this.head;
        this.head = newNode;
        this.size = this.size + 1;
    }

    public void printList() {
        Node newNode = this.head;
        while (newNode != null) {
            System.out.print(newNode.data + " ");
            newNode = newNode.next;
        }
        System.out.println("");
    }

    public void deleteSalahSatuNode() {
        Node tail = null;
        if (head == null) {
            return;
        } else {
            if (head == tail) {
                head = null;
                tail = null;
            } else {
                head = head.next;
            }
        }
    }

    public void deleteNode(int posisi) {
        if (head == null) {
            return;
        }
        Node current = head;
        if (posisi == 1) {
            head = current.next;
            return;
        }
        for (int i = 0; current != null && i < posisi - 1; i++) {
            current = current.next;
        }

        Node next = current.next.next;
        current.next = next;
    }

    public void deleteLastNode() {
        if (this.head == null) {
            return;
        }

        Node last = this.head;
        Node tmp = null;

        while (last.next != null) {
            tmp = last;
            last = last.next;
        }
        tmp.next = null;
    }

    public int getSize() {
        return this.size;
    }
}


/*1. Menampilkan isi LinkedList
2. Mengapus Salah Satu Node Linked List
3. Menghapus Node di Akhir List
4. Menghapus Node Sesuai Target*/
