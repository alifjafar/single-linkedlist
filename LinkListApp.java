
class LinkListApp {

    public static void main(String[] args) {

        LinkList ll = new LinkList();

        ll.insert(10);
        ll.insert(12);
        ll.insert(15);

        System.out.print("Total Size = ");
        System.out.println(ll.getSize());

        System.out.print("Isi Node : ");
        ll.printList();

        System.out.println("\nMenghapus Salah Satu Node");
        ll.deleteSalahSatuNode();
        System.out.print("Isi Node : ");
        ll.printList();

        System.out.println("Menghapus Baris Akhir : ");
        ll.deleteLastNode();
        System.out.print("Isi Node : ");
        ll.printList();

        ll.deleteNode(1);
        System.out.println("Menghapus Node Position 1");
        System.out.print("Isi Node : ");
        ll.printList();

    }
}
